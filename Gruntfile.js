module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            client: {
                src: [
                    'public/js/*.js',
                ],
                options: {
                    globals: {
                        jQuery: true,
                        console:true,
                        module: true,
                        strict: true,
                    }
                }
            }
        },
        concat: {
            client: {
                options: {
                    separator: '\n'
                },
                files: {
                    'public/js/boilerplate.js': 'public/src/js/*.js',
                    'public/css/boilerplate.css': 'public/src/css/*.css'
                }
            }
        },
        uglify: {
            client: {
                files: {
                    'public/js/boilerplate.min.js': 'public/src/js/boilerplate.js'
                }
            }

        },
        copy: {
            client: {
                files: [
                    {
                        expand: true,
                        cwd: 'bower_components/bootstrap/dist/js',
                        src: ['bootstrap.min.js'],
                        dest: 'public/js/libs'
                    },
                    {
                        expand: true,
                        cwd: 'bower_components/jquery/dist',
                        src: ['jquery.min.js'],
                        dest: 'public/js/libs'
                    },
                    {
                        expand: true,
                        cwd: 'bower_components/bootstrap/dist/css',
                        src: ['bootstrap.min.css'],
                        dest: 'public/css'
                    },
                    {
                        expand: true,
                        cwd: 'bower_components/bootstrap/dist/css',
                        src: ['bootstrap-theme.min.css'],
                        dest: 'public/css'
                    }
                ]
            }
        },
        watch: {
            scripts: {
                files: ['public/src/js/*.js', 'public/src/css/*.css'],
                tasks: ['client'],
                options: {
                spawn: false,
                },
            },
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('client', ['jshint:client', 'copy:client', 'concat:client', 'uglify:client']);

}